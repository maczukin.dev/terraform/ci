export VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
export REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
export BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)

ALPINE_VERSION := 3.13
TF_VERSIONS := 0.14.5 0.14.7 1.0.3 1.3.4
TFLINT_VERSION := v0.24.1

RELEASE_INDEX_GEN_VERSION ?= master
releaseIndexGen ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)

CI_REGISTRY_IMAGE ?= terraform
CI_COMMIT_REF_NAME ?= HEAD
CI_COMMIT_SHA ?= HEAD

.PHONY: build-terraform-image
build-terraform-image:
	docker build \
      --build-arg ALPINE_VERSION="$(ALPINE_VERSION)" \
      --build-arg TF_VERSIONS="$(TF_VERSIONS)" \
      --build-arg TFLINT_VERSION="$(TFLINT_VERSION)" \
      -t $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_NAME) \
      -f dockerfiles/ci/Dockerfile \
      dockerfiles/ci
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_REF_NAME)
	docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

.PHONY: build-gitlab-ci-yaml-files
build-gitlab-ci-yaml-files:
	mkdir -p build/
	jsonnet --string --ext-str imageVersion=$(CI_COMMIT_REF_NAME) ./source/base.gitlab-ci.jsonnet > build/base.gitlab-ci.yml
	jsonnet --string --ext-str imageVersion=$(CI_COMMIT_REF_NAME) ./source/modules-lint.gitlab-ci.jsonnet > build/modules-lint.gitlab-ci.yml
	jsonnet --string --ext-str imageVersion=$(CI_COMMIT_REF_NAME) ./source/deployment.gitlab-ci.jsonnet > build/deployment.gitlab-ci.yml

.PHONY: jsonnet-fmt
jsonnet-fmt:
	find ./source/ -name "*.jsonnet" -exec jsonnetfmt -i {} \;
	find ./source/ -name "*.libsonnet" -exec jsonnetfmt -i {} \;

.PHONY: jsonnet-check-fmt
jsonnet-check-fmt:
	$(MAKE) jsonnet-fmt
	git --no-pager diff --compact-summary --exit-code -- ./source/

latest_stable_tag := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)

.PHONY: release_s3
release_s3: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_s3: CI_COMMIT_SHA ?= $(REVISION)
release_s3: S3_BUCKET ?=
release_s3:
	@$(MAKE) index_file
ifneq ($(S3_BUCKET),)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)/"
ifeq ($(shell git describe --exact-match --match $(latest_stable_tag) >/dev/null 2>&1; echo $$?), 0)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/latest/"
endif
endif

.PHONY: sync_s3_release
sync_s3_release: S3_URL ?=
sync_s3_release:
	# Syncing with $(S3_URL)
	@aws --endpoint-url ${S3_ENDPOINT_URL} s3 sync build "$(S3_URL)" --acl public-read

.PHONY: remove_s3_release
remove_s3_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_s3_release: S3_BUCKET ?=
remove_s3_release:
ifneq ($(S3_BUCKET),)
	@aws --endpoint-url ${S3_ENDPOINT_URL} s3 rm "s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)" --recursive
endif

.PHONY: index_file
index_file: export CI_COMMIT_REF_NAME ?= $(BRANCH)
index_file: export CI_COMMIT_SHA ?= $(REVISION)
index_file: $(releaseIndexGen)
	# generating index.html file
	@$(releaseIndexGen) \
		-working-directory build/ \
		-project-version $(VERSION) \
		-project-git-ref $(CI_COMMIT_REF_NAME) \
		-project-git-revision $(CI_COMMIT_SHA) \
		-project-name "Terraform CI configuration" \
		-project-repo-url "https://gitlab.com/maczukin.dev/terraform/ci"

$(releaseIndexGen): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(releaseIndexGen): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(releaseIndexGen):
	# Installing $(DOWNLOAD_URL) as $(releaseIndexGen)
	@mkdir -p $(shell dirname $(releaseIndexGen))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(releaseIndexGen)"
	@chmod +x "$(releaseIndexGen)"

.PHONY: prepare_development_image
prepare_development_image: CI_IMAGE ?= terraform-ci-development
prepare_development_image: CI_REGISTRY ?= ""
prepare_development_image:
	# Building the $(CI_IMAGE) image
	docker build \
			--pull \
			--no-cache \
			--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
			-t $(CI_IMAGE) \
			-f dockerfiles/development/Dockerfile \
			dockerfiles/ci/
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker push $(CI_IMAGE)
	docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif
