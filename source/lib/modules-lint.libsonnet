local base = import './base.libsonnet';

local stageValidate = 'validate';

local stages = [
  stageValidate,
];

local format = {
  stage: stageValidate,
  extends: [
    base.formatJobName,
  ],
};

local validate = {
  stage: stageValidate,
  extends: [
    base.validateJobName,
  ],
};

{
  stages: stages,
  jobs: {
    'terraform format': format,
    'terraform validate': validate,
  },
}
