local base = import './base.libsonnet';

local stageBuild = 'build';
local stageDeploy = 'deploy';

local stages = [
  stageBuild,
  stageDeploy,
];

local cache = {
  key: 'terraform-${CI_COMMIT_REF_SLUG}',
  paths: [
    '.terraform',
  ],
};

local variables = {
  TF_PLAN: 'tfplan.cache',
  TF_PLAN_JSON: 'tfplan.json',
  TF_STATE_NAME: 'production',
};

local plan = {
  stage: stageBuild,
  extends: [
    base.initJobName,
  ],
  script: [
    'terraform plan -out="${TF_PLAN}"',
    'convert_plan_to_json "${TF_PLAN}" > "${TF_PLAN_JSON}"',
  ],
  artifacts: {
    paths: [
      '${TF_PLAN}',
    ],
    reports: {
      terraform: '${TF_PLAN_JSON}',
    },
  },
};

local apply = {
  stage: stageDeploy,
  extends: [
    base.initJobName,
  ],
  dependencies: [
    'terraform plan',
  ],
  rules: [
    {
      'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
      when: 'manual',
    },
  ],
  script: [
    'terraform apply -input=false -auto-approve "${TF_PLAN}"',
  ],
};

{
  stages: stages,
  cache: cache,
  variables: variables,
  jobs: {
    'terraform plan': plan,
    'terraform apply': apply,
  },
}
