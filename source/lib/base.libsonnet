local imageVersion = std.extVar('imageVersion');

local warning = "# Generated automatically with 'make build-gitlab-ci-yaml-files' - do not hand edit!\n\n";

local baseJobName = '.tf_base';
local initJobName = '.tf_init';
local formatJobName = '.tf_format';
local validateJobName = '.tf_validate';

local baseBeforeScript = [
  'if [ ! -s ".terraform-version" ]; then echo "Missing .terraform version file"; exit 1; fi',
  'tfenv install',
  'terraform version',
];

local base = {
  image: std.format('registry.gitlab.com/maczukin.dev/terraform/ci:%s', imageVersion),
  before_script: baseBeforeScript,
};

local init = {
  extends: [
    baseJobName,
  ],
  before_script: baseBeforeScript + [
    'export TF_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"',
    'terraform init ' +
    '    -backend-config="address=${TF_ADDRESS}" ' +
    '    -backend-config="lock_address=${TF_ADDRESS}/lock" ' +
    '    -backend-config="lock_method=POST" ' +
    '    -backend-config="unlock_address=${TF_ADDRESS}/lock" ' +
    '    -backend-config="unlock_method=DELETE" ' +
    '    -backend-config="username=gitlab-ci-token" ' +
    '    -backend-config="password=${CI_JOB_TOKEN}" ' +
    '    -backend-config="retry_wait_min=5" ' +
    '    -reconfigure',
  ],
};

local format = {
  extends: [
    baseJobName,
  ],
  script: [
    'find . -name "*.tf" | xargs -I{} terraform fmt {}',
    'git --no-pager diff --compact-summary --exit-code',
  ],
};

local validate = {
  extends: [
    baseJobName,
  ],
  script: [
    'terraform init -backend=false -reconfigure',
    'terraform validate .',
    'tflint',
  ],
};

{
  warning: warning,

  baseJobName: baseJobName,
  initJobName: initJobName,
  formatJobName: formatJobName,
  validateJobName: validateJobName,

  jobs: {
    [std.format('%s', baseJobName)]: base,
    [std.format('%s', initJobName)]: init,
    [std.format('%s', formatJobName)]: format,
    [std.format('%s', validateJobName)]: validate,
  },
}
