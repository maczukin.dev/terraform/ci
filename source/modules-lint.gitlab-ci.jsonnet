local base = import './lib/base.libsonnet';
local modulesLint = import './lib/modules-lint.libsonnet';

local template =
  {
    stages: modulesLint.stages,
  } +
  base.jobs +
  modulesLint.jobs;

base.warning + std.manifestYamlDoc(template)
