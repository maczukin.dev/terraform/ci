local base = import './lib/base.libsonnet';
local deployment = import './lib/deployment.libsonnet';
local modulesLint = import './lib/modules-lint.libsonnet';

local template =
  {
    stages: modulesLint.stages + deployment.stages,
    cache: deployment.cache,
    variables: deployment.variables,
  } +
  base.jobs +
  modulesLint.jobs +
  deployment.jobs;

base.warning + std.manifestYamlDoc(template)
